FROM python:3.8

WORKDIR /app

COPY requirements.txt .

RUN pip install -r requirements.txt

COPY . .

CMD ["flake8", "."]

CMD ["pytest"]

CMD ["uvicorn", "main:app", "--host", "0.0.0.0", "--port", "8000"]

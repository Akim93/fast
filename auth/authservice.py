from sqlalchemy.orm import Session
from fastapi import Depends, HTTPException, status
from datetime import datetime, timedelta
from models import User
from jose import JWTError, jwt
from fastapi.security import OAuth2PasswordBearer
oauth2_scheme = OAuth2PasswordBearer(tokenUrl="token")
from database import get_db
from auth.auth_token_service import AuthTokenService


class AuthService:

    @staticmethod
    def login_user(username: str, password: str, db: Session):
        user = db.query(User).filter(User.username == username, User.password == password).first()

        if user is None:
            raise HTTPException(
                status_code=status.HTTP_401_UNAUTHORIZED,
                detail="Invalid credentials",
                headers={"WWW-Authenticate": "Bearer"},
            )
        
        access_token = AuthTokenService.create_access_token(data={"sub": username})
        return {"access_token": access_token, "token_type": "bearer"}


    @staticmethod
    def create_user(username: str, password: str, db: Session):
        existing_user = db.query(User).filter(User.username == username).first()
        if existing_user:
            raise HTTPException(status_code=400, detail=f"Username {username} already registered")

        new_user = User(username=username, password=password)
        db.add(new_user)
        db.commit()
        db.refresh(new_user)

        return new_user


    @staticmethod
    def get_current_user_from_token(token: str = Depends(oauth2_scheme), db: Session = Depends(get_db)):
        credentials_exception = HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Could not validate credentials",
            headers={"WWW-Authenticate": "Bearer"},
        )
        try:
            payload = AuthTokenService.decode_access_token(token)
            username: str = payload.get("sub")
            if username is None:
                raise credentials_exception
        except JWTError as e:
            raise credentials_exception

        user = db.query(User).filter(User.username == username).first()
        if user is None:
            raise credentials_exception
        return user

from fastapi import APIRouter, HTTPException, Form
from fastapi import Depends
from sqlalchemy.orm import Session
from .authservice import AuthService
from database import get_db
from models import UserResponse
from models import User


router = APIRouter()


@router.post("/token")
async def login_for_access_token(username: str = Form(...), password: str = Form(...), db: Session = Depends(get_db)):
    return AuthService.login_user(username, password, db)


@router.post("/register", response_model=UserResponse)
def register_user(username: str = Form(...), password: str = Form(...), db: Session = Depends(get_db)):
    return AuthService.create_user(username, password, db)


@router.get("/users/me", response_model=UserResponse)
async def read_users_me(current_user: User = Depends(AuthService.get_current_user_from_token)):
    return current_user

from fastapi import Depends, HTTPException, status
from models import User
from .authservice import AuthService

def is_admin(current_user: User = Depends(AuthService.get_current_user_from_token)):
	if not current_user.is_admin:
		raise HTTPException(
			status_code=status.HTTP_403_FORBIDDEN,
			detail="You do not have access to this resource",
		)
	return current_user

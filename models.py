from sqlalchemy import Column, Integer, String, DateTime, ForeignKey, Boolean, Text
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship
from datetime import datetime
from pydantic import BaseModel
from typing import List
from typing import Optional

Base = declarative_base()



class PhotoBase(BaseModel):
    url: str

class ItemBase(BaseModel):
    name: str
    description: str



class ItemResponse(ItemBase):
    id: int
    created_at: datetime
    mini_photo: Optional[str]
    price: int
    big_text: str
    photos: Optional[List[PhotoBase]]

    class Config:
        from_attributes = True
        orm_mode = True

class ItemCreate(BaseModel):
    name: str
    description: str

class Category(Base):
    __tablename__ = "categories"
    id = Column(Integer, primary_key=True, index=True)
    name = Column(String, index=True)
    items = relationship("Item", back_populates="category")

class Item(Base):
    __tablename__ = "items"
    id = Column(Integer, primary_key=True, index=True)
    name = Column(String, index=True)
    description = Column(String, index=True)
    big_text = Column(Text, nullable=True)
    price = Column(Integer)
    created_at = Column(DateTime, default=datetime.utcnow)
    category_id = Column(Integer, ForeignKey("categories.id"))
    category = relationship("Category", back_populates="items")
    photos = relationship("Photo", back_populates="item")
    mini_photo = Column(String, index=True)

class Photo(Base):
    __tablename__ = "photos"
    id = Column(Integer, primary_key=True, index=True)
    url = Column(String, index=True)
    item_id = Column(Integer, ForeignKey("items.id"))
    item = relationship("Item", back_populates="photos")


class User(Base):
    __tablename__ = "users"
    id = Column(Integer, primary_key=True, index=True)
    username = Column(String, unique=True, index=True)
    password = Column(String)
    is_active = Column(Boolean, default=True)
    is_admin = Column(Boolean, default=False)


class UserResponse(BaseModel):
    username: str
    
class Role(Base):
    __tablename__ = "roles"
    id = Column(Integer, primary_key=True, index=True)
    name = Column(String, unique=True, index=True)

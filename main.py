from fastapi import FastAPI, HTTPException, UploadFile, File, Form
from sqlalchemy import create_engine, Column, Integer, String, DateTime, ForeignKey
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker, joinedload
from datetime import datetime
from pydantic import BaseModel
from sqlalchemy.orm import relationship
from typing import Optional, List

from auth import authrouter
from admin import adminrouter
from product import productrouter

from models import Item, ItemResponse, Category, Photo
import os

from database import SessionLocal, check_tables_exist, create_database
from fastapi.staticfiles import StaticFiles
from fastapi.middleware.cors import CORSMiddleware


if not check_tables_exist():
    create_database()

app = FastAPI()

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

app.include_router(authrouter.router, prefix="/api/v1/auth")
app.include_router(adminrouter.router, prefix="/api/v1/admin")

app.include_router(productrouter.router, prefix="/api/v1/product")
app.mount("/images", StaticFiles(directory="images"), name="static")

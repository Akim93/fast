from sqlalchemy.orm import Session
from fastapi import Depends, HTTPException, status
from datetime import datetime, timedelta
from models import User
from jose import JWTError, jwt
from fastapi.security import OAuth2PasswordBearer
oauth2_scheme = OAuth2PasswordBearer(tokenUrl="token")
from database import get_db
from auth.auth_token_service import AuthTokenService
from typing import List
from fastapi import UploadFile
from models import Item, ItemResponse, Category, Photo, UserResponse
from fastapi import File, UploadFile
import os
from datetime import datetime


class AdminService:


    @staticmethod
    def create_item(name: str, description: str, big_text: str, price: int, category_id: int, files: List[UploadFile], pic: UploadFile, db: Session):
        category = db.query(Category).filter(Category.id == category_id).first()
        if not category:
            raise HTTPException(status_code=404, detail="Category not found")

        item_data = {"name": name, "description": description, "big_text": big_text, "price": price, "category_id": category_id}
        db_item = Item(**item_data)
        db.add(db_item)
        db.flush()
        print("____________________________________________________________")
        print(db_item.id)

        for file in files:
            file_path = AdminService.create_upload_file(file)
            db_photo = Photo(url=file_path, item_id=db_item.id)
            db.add(db_photo)

        db_item.photos = sorted(db.query(Photo).filter(Photo.item_id == db_item.id).all(), key=lambda photo: photo.id)

        pic_path = AdminService.create_upload_file(pic)


        db_item.mini_photo = pic_path

        db.commit()
        db.refresh(db_item)
        return db_item

    @staticmethod
    def create_upload_file(file: UploadFile = File(...)):
        upload_folder = "images"
        os.makedirs(upload_folder, exist_ok=True)

        current_datetime = datetime.now()

        subfolder_name = current_datetime.strftime("%Y-%m-%d_%H")
        upload_subfolder = os.path.join(upload_folder, subfolder_name)
        os.makedirs(upload_subfolder, exist_ok=True)

        unique_filename = f"{current_datetime.strftime('%Y-%m-%d_%H-%M-%S')}_{file.filename}"

        file_path = os.path.join(upload_subfolder, unique_filename)

        with open(file_path, "wb") as f:
            f.write(file.file.read())

        return file_path


    @staticmethod
    def edit_item(item_id: int, name: str, description: str, big_text: str, price: int, category_id: int, files: List[UploadFile], pic: UploadFile, db: Session):

        db_item = db.query(Item).filter(Item.id == item_id).first()
        if not db_item:
            raise HTTPException(status_code=404, detail="Item not found")

        db_item.name = name
        db_item.description = description
        db_item.big_text = big_text
        db_item.price = price
        db_item.category_id = category_id

        for file in files:
            file_path = AdminService.create_upload_file(file)
            db_photo = Photo(url=file_path, item_id=item_id)
            db.add(db_photo)

        pic_path = AdminService.create_upload_file(pic)
        db_item.mini_photo =  pic_path

        db.commit()
        db.refresh(db_item)
        
        return db_item


    @staticmethod
    def delete_item(item_id: int, db: Session):
        item = db.query(Item).filter(Item.id == item_id).first()
        if not item:
            raise HTTPException(status_code=404, detail="Item not found")
        db.delete(item)
        db.commit()
        return item
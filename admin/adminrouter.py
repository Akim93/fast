from fastapi import APIRouter, HTTPException, Form
from fastapi import Depends
from sqlalchemy.orm import Session
from database import get_db
from models import UserResponse
from models import User
from fastapi.security import OAuth2PasswordBearer
oauth2_scheme = OAuth2PasswordBearer(tokenUrl="token")
from fastapi import Depends, HTTPException, status
from jose import JWTError, jwt
from auth.auth_token_service import AuthTokenService
from admin.adminservice import AdminService

from auth.role import is_admin
from fastapi import File, UploadFile
from typing import List
from models import ItemResponse


router = APIRouter()


@router.post("/create", dependencies=[Depends(is_admin)], response_model=ItemResponse)
def admin_page(name: str = Form(...), description: str = Form(...), big_text: str = Form(...), price: int = Form(...), category_id: int = Form(...), files: List[UploadFile] = File(...), pic: UploadFile = File(...), db: Session = Depends(get_db)):
    return AdminService.create_item(name, description, big_text, price, category_id, files, pic, db)


@router.put("/edit/{item_id}", dependencies=[Depends(is_admin)], response_model=ItemResponse)
def admin_page(item_id: int, name: str = Form(...), description: str = Form(...), big_text: str = Form(...), price: int = Form(...), category_id: int = Form(...), files: List[UploadFile] = File(...), pic: UploadFile = File(...), db: Session = Depends(get_db)):
    return AdminService.edit_item(item_id, name, description, big_text, price, category_id, files, pic, db)


@router.delete("/delete/{item_id}", dependencies=[Depends(is_admin)])
def delete_item(item_id: int, db: Session = Depends(get_db)):
    try:
        deleted_item = AdminService.delete_item(item_id, db)
        return {"message": f"Item with id {item_id} has been successfully deleted"}
    except HTTPException as e:
        return e

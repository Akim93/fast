# database.py
from sqlalchemy import create_engine, inspect
from sqlalchemy.orm import sessionmaker
from models import Item, Category, Photo, User, Role
from sqlalchemy import inspect
from models import Base

DATABASE_URL = "sqlite:///./test.db"
engine = create_engine(DATABASE_URL, echo=True)
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)


def check_tables_exist():
    inspector = inspect(engine)
    existing_tables = inspector.get_table_names()
    
    expected_tables = [Item.__table__.name, Category.__table__.name, Photo.__table__.name, User.__table__.name, Role.__table__.name]
    
    for table_name in expected_tables:
        if table_name not in existing_tables:
            print(f"Table '{table_name}' does not exist.")
            return False

    print("All tables exist.")
    return True


def create_database():
    Base.metadata.create_all(bind=engine)
    print("Database created.")


def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()

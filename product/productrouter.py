from fastapi import APIRouter, HTTPException, Form
from fastapi import Depends
from sqlalchemy.orm import Session
from .productservice import ProductService
from database import get_db
from models import UserResponse, User, ItemResponse
from typing import Optional, List

router = APIRouter()


@router.get("/items/", response_model=List[ItemResponse])
def read_items(db: Session = Depends(get_db), category_id: Optional[int] = None, skip: int = 0, limit: int = 50):
    return ProductService.read_items(db, category_id, skip, limit)


@router.get("/detail/{item_id}", response_model=ItemResponse)
def read_item(item_id: int, db: Session = Depends(get_db)):
	return ProductService.read_item(item_id, db)

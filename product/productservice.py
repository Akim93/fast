from sqlalchemy.orm import Session
from fastapi import Depends, HTTPException, status
from datetime import datetime, timedelta
from models import User, Item
from jose import JWTError, jwt
from fastapi.security import OAuth2PasswordBearer
oauth2_scheme = OAuth2PasswordBearer(tokenUrl="token")
from database import get_db
from auth.auth_token_service import AuthTokenService
from typing import Optional
from sqlalchemy import desc


class ProductService:

    @staticmethod
    def read_items(db: Session, category_id: Optional[int] = None, skip: int = 0, limit: int = 50):
        items_query = db.query(Item)

        if category_id:
            items_query = items_query.filter(Item.category_id == category_id)
        
        items = items_query.order_by(desc(Item.created_at)).offset(skip).limit(limit).all()
        # print(items[0].mini_photo)
        # items = items_query.order_by(desc(Item.created_at)).offset(skip).limit(limit).all()
        # db.close()
        # for item in items:
        #     item.mini_photos_urls = [mini_photo.url for mini_photo in item.mini_photo]

        # for item in items:
        #     mini_photo_urls = [mini_photo.url for mini_photo in item.mini_photo] if item.mini_photo else None
        #     item.mini_photo = MiniPhotoBase(url=mini_photo_urls[0]) if mini_photo_urls else None



        # print(items)
        
        return items

    @staticmethod
    def read_item(item_id: int, db: Session):
        with db.begin():
            db_item = db.query(Item).filter(Item.id == item_id).first()
        # db.close()
        if db_item is None:
            raise HTTPException(status_code=404, detail="Item not found")
        return db_item
